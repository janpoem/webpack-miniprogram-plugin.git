const fs = require('fs');
const pathUtil = require('path');
const colors = require('ansi-colors');
const _ =  require('lodash');

const readJsonSync = (file) => {
	try {
		const data = fs.readFileSync(file);
		return JSON.parse(data);
	} catch (e) {

	}
	return {};
};

const filterExts = (exts) => _.keys(exts).filter(ext => ext !== '' && !!exts[ext]);

const replaceDS = (path) => path.replace(/[\\/]+/g, '/');
const removeExt = (path) => {
	const ext = pathUtil.extname(path);
	if (ext !== '') {
		const reg = new RegExp(`${ext}$`);
		return path.replace(reg, '');
	}
	return path;
};

const filterExcludeItems = (items, excludes) => {
	excludes = _.uniq(_.flattenDeep(excludes));
	return items.filter(item => excludes.indexOf(item) < 0);
};

const warn = (msg) => {
	return colors.yellowBright(msg);
};

const error = (msg) => {
	return colors.bold.redBright(msg);
};

const highLight = (msg) => {
	return colors.underline.cyan(msg);
};

const right = (msg) => {
	return colors.bold.green(msg);
};

const filterMode = (mode) => {
	switch (mode.toLowerCase()) {
		case 'production' :
		case 'prod' :
			return 'production';
		default:
			return 'development';
	}
};

module.exports = {
	readJsonSync,
	filterExts,
	replaceDS,
	removeExt,
	filterExcludeItems,
	warn, error, highLight, right,
	filterMode
};
